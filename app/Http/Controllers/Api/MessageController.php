<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Message;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{

    public function showmess($rep_id)
    {
        // Lấy id người dùng đang đăng nhập
        $chat_id = Auth::id();
        $messages = Message::where(function ($query) use ($chat_id, $rep_id) {
            $query->where('chat_id', $chat_id)->where('rep_id', $rep_id);
        })->orWhere(function ($query) use ($chat_id, $rep_id) {
            $query->where('chat_id', $rep_id)->where('rep_id', $chat_id);
        })->orderBy('created_at', 'asc')->get();
        $formattedMessages = $messages->map(function ($message) {
            $user = User::find($message->rep_id);
            $userName = $user ? $user->name : '';

            return [
                'id' => $message->id,
                'content' => $message->content,
                'created_at' => $message->created_at,
                'chat_id' => $message->chat_id,
                'rep_id' => $message->rep_id,
                'rep_name' => $userName,
            ];
        });
        return response()->json([
            'data' => $formattedMessages,
        ]);
    }
    public function create(Request $request,$repId)
    {
        $chatId = Auth::id();
        $content = $request->input('content');
        $sender = User::find($chatId);
        $recipient = User::find($repId);
        $message = Message::create([
            'chat_id' => $chatId,
            'rep_id' => $repId,
            'content' => $content,
        ]);
        if ($message) {
            return response()->json([
                'status' => 'success',
                'message' => 'Message sent successfully',
                'data' => [
                    'message' => $message,
                    'sender' => $sender->name,
                    'recipient' => $recipient->name,
                ]
            ]);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Failed to send message'
            ], 500);
        }
    }

    public function update(Request $request, $id)
    {
        $userlogin = Auth::id();
        $content = $request->input('content');
        $message = Message::find($id);
        if (!$message) {
            return response()->json([
                'status' => 'error',
                'message' => 'Message not found'
            ], 404);
        }
        if ($message->chat_id !== $userlogin) {
            return response()->json([
                'status' => 'error',
                'message' => 'Bạn không được sửa tin nhắn này'
            ], 403);
        }
        $message->content = $content;
        $message->save();
        return response()->json([
            'status' => 'success',
            'message' => 'Sửa thành công',
            'data' => [
                'message' => $message,
            ]
        ]);
    }


    public function delete(Request $request, $id)
    {
        $Userlogin = auth()->user()->id;
        $message = Message::find($id);
        if (!$message) {
            return response()->json(['message' => 'Message not found'], 404);
        }
        // Kiểm tra xem id của người dùng đăng nhập trùng khớp với chat_id của tin nhắn
        if ($message->chat_id !== $Userlogin) {
            return response()->json(['message' => 'error'], 401);
        }
        $message->delete();
        return response()->json(['message' => 'success']);
    }

}
