<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Groups;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class GroupsController extends Controller
{
    //lấy danh sách user trong nhóm
    public function index()
    {
        $groups = Groups::with('user')->get();

        return response()->json([
            'status' => true,
            'message' => 'Groups retrieved successfully',
            'groups' => $groups,
        ], 200);
    }
    // thêm user mới vào nhóm
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|integer',
//            'message_content' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
//                'message' => 'Validation error',
                'errors' => $validator->errors()
            ], 422);
        }
        $user = User::find($request->input('user_id'));
        $group = Groups::create([
            'user_id' => $user->id,
//            'message_content' => $request->input('message_content')
        ]);
        return response()->json([
            'status' => true,
            'message' => 'Group created successfully',
            'group' => $group,
            'user_name' => $user->name,
        ], 201);
    }
    //xóa user khỏi nhóm
    public function delete($id)
    {
        $group = Groups::find($id);

        if (!$group) {
            return response()->json([
                'status' => false,
                'message' => 'Group not found',
            ], 404);
        }

        $group->delete();

        return response()->json([
            'status' => true,
            'message' => 'Group deleted successfully',
        ], 200);
    }

}
