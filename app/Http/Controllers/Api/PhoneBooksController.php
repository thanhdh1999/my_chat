<?php

namespace App\Http\Controllers\Api;
use App\Models\Message;
use App\Models\PhoneBooks;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class PhoneBooksController extends Controller
{
    public  function show(){
        $phonebooks = PhoneBooks::with('user')->get();
        $data = [];
        foreach ($phonebooks as $phonebook) {
            $data[] = [
                'user_id' => $phonebook->user_id,
                'name' => $phonebook->user->name,
                'phone_number' => $phonebook->number_phone,
                'address' => $phonebook->user->address,
                'email' => $phonebook->user->email,
            ];
        }
        return response()->json([
            'data' => $data,
        ]);
    }
    public function create(Request $request)
    {
        $user = User::findOrFail($request->input('user_id'));
        $request->validate([
            'user_id' => 'required|exists:users,id',
            'status' => 'required|in:1,2'
        ]);
        $phoneBook = PhoneBooks::create([
            'user_id' => $user->id,
            'status' => $request->status,
        ]);
        return response()->json([
            'message' => 'Phone book created successfully',
            'data' => $phoneBook,
        ]);
    }

    public function delete($id)
    {
        // Tìm và xóa bản ghi danh bạ theo ID
        $phonebook = PhoneBooks::find($id);
        if (!$phonebook) {
            return response()->json([
                'status' => false,
                'message' => 'Phonebook not found',
            ], 404);
        }
        $phonebook->delete();
        return response()->json([
            'status' => true,
            'message' => 'Phonebook deleted successfully',
        ], 200);
    }
    public function details($user_id)
    {
        $phonebook = PhoneBooks::whereHas('user', function ($query) use ($user_id) {
            $query->where('user_id', $user_id);
        })->first();
        if ($phonebook) {
            $data = [
                'id'=> $phonebook->id,
                'user_id' => $phonebook->user_id,
                'name' => $phonebook->user->name,
                'phone' => $phonebook->user->phone,
                'address' => $phonebook->user->address,
                'email' => $phonebook->user->email,
            ];
            return response()->json([
                'data' => $data,
            ]);
        }
        return response()->json([
            'error' => 'Phonebook not found.',
        ], 404);
    }


}
