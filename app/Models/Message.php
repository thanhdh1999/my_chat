<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;
    protected $table = 'messages';

    protected $fillable = ['chat_id', 'rep_id', 'group_id', 'content'];

    public function receivedMessages()
    {
        return $this->hasMany(User::class, 'rep_id');
    }

}
