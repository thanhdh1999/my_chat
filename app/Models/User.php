<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
class User extends Authenticatable
{
    use HasFactory,HasApiTokens;
    protected $table = 'users';

    protected $fillable = ['name', 'email', 'password', 'address', 'phone', 'image', 'status'];

    // Relationship with PhoneBook model
    public function phoneBook()
    {
        return $this->hasOne(PhoneBook::class, 'user_id');
    }
    // Relationship with Group model
    public function groups()
    {
        return $this->hasMany(Group::class, 'user_id');
    }
    public function repMessages()
    {
        return $this->hasMany(Message::class, 'rep_id');
    }
    public function chatMessages()
    {
        return $this->hasMany(Message::class, 'chat_id');
    }
    public static function getUsersWithMessages()
    {
        // Lấy id người dùng đang đăng nhập
        $loggedInUserId = auth()->id();
        return self::where(function ($query) use ($loggedInUserId) {
            $query->whereHas('repMessages', function ($subQuery) use ($loggedInUserId) {
                $subQuery->where('chat_id', $loggedInUserId);
            });
        })->orWhere(function ($query) use ($loggedInUserId) {
            $query->whereHas('chatMessages', function ($subQuery) use ($loggedInUserId) {
                $subQuery->where('rep_id', $loggedInUserId);
            });
        })->get();
    }

}
