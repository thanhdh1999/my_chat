<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('address')->nullable();
            $table->string('phone');
            $table->json('image')->nullable();
            $table->boolean('status');
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('phone_books', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger ('user_id');
            $table->timestamps();
            $table->softDeletes();
            $table->boolean('status');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('groups', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger ('user_id');
            $table->text('message_content')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('messages', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger ('chat_id');
            $table->unsignedBigInteger ('rep_id');
            $table->unsignedBigInteger ('group_id');
            $table->text('content');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('chat_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('rep_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('messages');
        Schema::dropIfExists('groups');
        Schema::dropIfExists('phone_books');
        Schema::dropIfExists('users');
    }
};
