<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\PhoneBooks;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Message;
use App\Models\Groups;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        User::factory()->count(10)->create();
        PhoneBooks::factory()->count(10)->create();
        Groups::factory()->count(10)->create();
        Message::factory()->count(30)->create();

    }
}
