<?php

namespace Database\Seeders;

use App\Models\PhoneBooks;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PhoneBooksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {


        PhoneBooks::factory()->count(20)->create();
    }
}
