<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\DB;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class MessageFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $userIds = DB::table('users')->pluck('id')->toArray();
        $groupIds = DB::table('groups')->pluck('id')->toArray();

        $chatId = $this->faker->randomElement($userIds);
        $repId = $this->faker->randomElement($userIds);

        return [
            'chat_id' => $chatId,
            'rep_id' => $repId,
            'group_id' => $this->faker->randomElement($groupIds),
            'content' => $this->faker->text,
        ];
    }


}
