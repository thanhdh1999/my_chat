<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
class UserFactory extends Factory
{

    public function definition(): array
    {
        return [

            'name' => $this->faker->name(),
            'email' => $this->faker->email(),
            'email_verified_at' => now(),
            'password' => bcrypt('12345678'),
            'address' => $this->faker->address(),
            'phone' => $this->faker->phoneNumber(),
            'image' =>null,
            'status' => $this->faker->randomElement([1, 2]),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
