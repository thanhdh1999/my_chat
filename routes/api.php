<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\GroupsController;
use App\Http\Controllers\Api\MessageController;
use App\Http\Controllers\Api\PhoneBooksController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
Route::post('/auth/register', [AuthController::class, 'createUser']);
Route::post('/auth/login', [AuthController::class, 'loginUser']);

Route::middleware('auth:sanctum')->group(function () {
    Route::group(['prefix' =>'message'], function () {
        Route::get('/{rep_id}', [MessageController::class, 'showmess'])->name('show');
        Route::post('/create/{rep_id}', [MessageController::class, 'create'])->name('create');
        Route::post('/update/{id}', [MessageController::class, 'update'])->name('update');
        Route::delete('/delete/{id}', [MessageController::class, 'delete']);
    });
    Route::group(['prefix' =>'phonebooks'], function () {
        Route::get('/', [PhoneBooksController::class, 'show'])->name('show');
        Route::post('/create', [PhoneBooksController::class, 'create'])->name('create');
        Route::delete('/delete/{id}', [PhoneBooksController::class, 'delete']);
        Route::get('/details/{id}', [PhoneBooksController::class, 'details']);
    });
    Route::group(['prefix' =>'user'], function () {
        Route::get('/', [AuthController::class, 'index']);
        Route::delete('/delete/{id}', [AuthController::class, 'delete']);
        Route::get('/detail/', [AuthController::class, 'detail']);
        Route::post('/edit', [AuthController::class, 'update']);
        Route::get('/search', [AuthController::class, 'search']);
    });
    Route::group(['prefix' =>'groups'], function () {
        Route::get('/', [GroupsController::class, 'index']);
        Route::post('/create', [GroupsController::class, 'create']);
        Route::delete('/delete/{id}', [GroupsController::class, 'delete']);
    });

});
